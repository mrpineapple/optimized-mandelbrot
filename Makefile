CC=g++
CFLAGS=-c -Wall -ansi -msse3 -O3 -fopenmp -ftree-parallelize-loops=64 -fno-signed-zeros -fno-trapping-math -ftracer -funroll-loops --param max-unrolled-insns=140000 
LDFLAGS= 
SOURCES=main.cpp Screen.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=mandelbrot
LIBS=-lSDL -fopenmp

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) $(LIBS)  -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@ 



clean:
	rm -f *.o $(EXECUTABLE)
