#include <xmmintrin.h>                                  //SSE Instruction lib
#include <iostream>
#include <unistd.h>
#include <omp.h>
#include <sys/time.h>
#include "Screen.h"
using namespace std;

#define MAX_ITS 1000      	
#define HXRES 700 			
#define HYRES 700		   
#define MAX_DEPTH  40  		
#define ZOOM_FACTOR 1.02	
#define PX -0.702295281061	
#define PY 0.350220783400	

unsigned int pal[]={	                            //colour pallette
	255,180,4,                                      //the compiler prefers arrays of size 2^n
	240,156,4,
	220,124,4,
	156,71,4,
	72,20,4,
	251,180,4,
	180,74,4,
	180,70,4,
	164,91,4,
	100,28,4,
	191,82,4,
	47,5,4,
	138,39,4,
	81,27,4,
	192,89,4,
	61,27,4,
	216,148,4,
	71,14,4,
	142,48,4,
	196,102,4,
	58,9,4,
	132,45,4,
	95,15,4,
	92,21,4,
	166,59,4,
	244,178,4,
	194,121,4,
	120,41,4,
	53,14,4,
	80,15,4,
	23,3,4,
	249,204,4,
	97,25,4,
	124,30,4,
	151,57,4,
	104,36,4,
	239,171,4,
	131,57,4,
	111,23,4,
	4,2,4,
	0,0,0,0,0,0,0,0
};
float zoom_levels[] = {	                                        //hardcoded zoom values
	1,
	1.02,
	1.0404,
	1.06121,
	1.08243,
	1.10408,
	1.12616,
	1.14869,
	1.17166,
	1.19509,
	1.21899,
	1.24337,
	1.26824,
	1.29361,
 	1.31948,
 	1.34587,
 	1.37279,
	1.40024,
 	1.42825,
 	1.45681,
 	1.48595,
 	1.51567,
 	1.54598,
 	1.5769,
 	1.60844,
 	1.64061,
 	1.67342,
	1.70689,
 	1.74102,
 	1.77584,
 	1.81136,
 	1.84759,
 	1.88454,
 	1.92223,
 	1.96067,
 	1.99989,
 	2.03989,
 	2.08068,
	2.1223,
 	2.16474,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0
	};

unsigned int results[700][700];

int main()
{	
	                                                //variable declarations ordered by type
   	register unsigned int hx = 699;                 //hx is used an awful lot cuz primary loop variable. Keep it in a register
	unsigned int hy = 699;                          //may aswell initialize these to the value they will contain. Time is not being counted yet.
	unsigned int depth = MAX_DEPTH;
  	int i = 0; 
  	register int iterations = 0;
  	
  	unsigned long long total_time = 0;

	float m = 1;
  	float cx = 0;
  	float cy = 0;
    float chk[4] = {0,0,0,0};
	register float x = 0,y = 0;

	__m128 mulled;

	Screen *screen;
	screen = new Screen(HXRES, HYRES);

  	struct timeval start_time;
  	struct timeval stop_time;
	gettimeofday(&start_time, NULL);
	
	//GCC Prefers loops that descend. Expect to stay in the loop. No first clause needed.
	for (;__builtin_expect(depth != 0,1); --depth) {
		#pragma omp parallel for collapse(2) private(x,y,cy,cx,mulled,iterations,chk)   //parallelize the for loop. minimal set of varaiables privatized (most of em :( )
        //reducing to 0 again
		for (hy=699; hy>0; --hy) {			
			for (hx=699; hx>0; --hx) {
				mulled = _mm_mul_ps(_mm_setr_ps(700,2,hx,hy),_mm_setr_ps(m,1/m,4,4));		//result = (700m,2/m,4hx,4hy)
				mulled = _mm_rcp_ss(mulled);		                                        //         (1/700m,2/m,4hx,4hy)
				_mm_store_ps(chk,mulled);                                                   //
				//pipelined instructions                                                    //
				iterations = 1;                                                             //
				x = chk[0] * chk[2];                                                        // cx = 4hx/(HXRES*m) + PX - 2/m
				x = x + PX;                                                                 //
				x = x - chk[1];                                                             // cy = 4hy/(HXRES*m) + px - 2/m
				cx = x; 				                                                    //				
				y = chk[0] * chk[3];                                                        // x = cy and y = cy on every first iteration
				y = y + PY;
				y = y - chk[1];			
				cy = y;							
				do{       
					mulled = _mm_mul_ps(_mm_setr_ps(x,y,x+x,0),_mm_setr_ps(x,y,y,0));       //result = (x^2,y^2,2xy,0)
					_mm_store_ps(chk,mulled);	                                            //
					x = chk[0] + cx;					                                    //x = x^2 + cx - y^2 
					x = x - chk[1];                                                         //
					y = cy;                                                                 //y = 2xy + cy
					y = y + chk[2];									
				}while(__builtin_expect(((++iterations < MAX_ITS) && (chk[0]+chk[1]<4)),1));	
				//expect that conditiona will usually pass, also the conditions are ordered so the first one is more likely to fail.
				if(iterations != MAX_ITS){
					iterations=(iterations%40);                                             //altered i calculation
					i = --iterations << 1;                                                 
					i = i + iterations;
					results[hy][hx] = i;                                                    //store value for later
				}else{
				    results[hy][hx] = 0;
				}
			}
		}
		//reversing direction so that cache is 'still hot'
		#pragma omp parallel for collapse(2) private(i)                                     //hx and hy are automatically privatized 
		for(hy = 0; hy < HYRES; ++hy){
        		for(hx = 0; hx < HXRES; ++hx){
               			i = results[hy][hx];
                		if(i){
                    			screen->putpixel(hx, hy, pal[i], pal[i+1], 4);
                		}else{
                    			screen->putpixel(hx, hy, 0, 0, 0);
                		}
            		}
        	}
		screen->flip();                                                                      //update screen
		try{                                                                                 //if we've finished, throw an error to end the timing as soon as possible!
			if(depth == 1) throw 20;
		}catch(...){
			gettimeofday(&stop_time, NULL);
			total_time += (stop_time.tv_sec - start_time.tv_sec) * 1000000 + (stop_time.tv_usec - start_time.tv_usec);	
			cout << "Total executing time " << total_time << " microseconds\n";
		}
		m = zoom_levels[40-depth];                                                           //suprisingly, have the outmost forloop decrementing offsets this additional subtraction!
	}
}
